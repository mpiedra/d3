var current_data =[];
json_url= 'http://bdapp1.ccs.miami.edu/hpcjob/cj_stat/current';

// sacamos esta parte del código para afuera para no crear multiples svg en en DOM 
var svg = d3.select(".demo").append("svg:svg")
    .attr("width", 300)
    .attr("height", 200);

function load(url){
  // current_data = [];
  return new Promise(function(resolve, reject){
    $.getJSON(url, { get_param: 'value' }, function(data) {
      resolve(current_data.push(data));
    });
  });
}


setInterval(function(){
  load(json_url).then(function(result){
    console.log("Espero por el resultado");
    // var salida = current_data;
    console.log(current_data)
    showPoint(current_data);
  }).catch(function(){
    console.log("ocurrio un error");
  });
}, 60000);


function showPoint(info){
  var datos = info.slice();
  var amountFn = function(d){return d.jRUN };
  var dateFn = function(d){ 
    return new Date(parseInt(d.ts))
  };

  var x = d3.time.scale()
    .range([10, 280])
    .domain(d3.extent(datos, dateFn))


  //extent is an alias to .domain([min, max])
  var y = d3.scale.linear()
    .range([180,10])
    .domain(d3.extent(datos, amountFn));

  // eliminar los datso es un pooco mas complicado, la solucion esta en
  // añadir una funcion clave a los datos 

  // data is bound to your selection
  var circle = svg.selectAll("circle").data(datos);

  // se aplica a los nodos que ya estan creados, no a los nuevos 
  circle.transition()
    .attr("cx", function(d){return x(dateFn(d)) })
    .attr("cy", function(d){return y(amountFn(d)) })


  // enter only affect ato new data
  circle.enter()
    .append("svg:circle")
    .attr("r", 4)
    .attr("cx", function(d){ return x(dateFn(d))})
    .attr("cy", function(d){ return y(amountFn(d))})

  // circle.exit().remove()

}