current_url = "http://bdapp1.ccs.miami.edu/uride/icd9_history/401.9?limit=5"; 
horizontalStackedBar(current_url);


function horizontalStackedBar(current_url){
    createPlot(current_url)
    function createPlot(current_url){
      url_json = current_url;
      load(url_json).then(function(result){
        render(all_data);
        console.log("Success plot");
      }).catch(function(){
        console.log("Error!");
      });
    }


    // all_codes
    function load(url){
      datos =[];
      order =[];
      return new Promise(function(resolve, reject){
        $.getJSON(url, {get_param: 'value'}, function(data){
          order = [">12m_b" , "6-12m_b", "<6m_b", "0", "<6m_a",  "6-12m_a", ">12m_a" ];
          var order_label = [{key: ">12m_b", value:"12 Month Before"} , {key: "6-12m_b", value:"6-12 Month Before"}, {key: "<6m_b", value:"6 Month Before"}, {key: "0", value:"Same Day"}, {key: "<6m_a", value:"6 Month After"},  {key: "6-12m_a", value:"6-12 Month After"}, {key:">12m_a", value:"12 Month After"} ];
          codes =[];
          order.map(function(item){
            data[item].map(function(elementos){
              if ($.inArray(elementos.code, codes) === -1){
                codes.push(elementos.code);
              }
            })
          })

          function createObj(){
            new_obj = {};
            new_obj.type = "";

            return new_obj;
          }

          all_data =[];
          order_label.map(function(item){
            var obj = [];
            for (var i = 0; i < data[item.key].length; i++) {
              // obj[data[item.key][i].code] = data[item.key][i].count;
              data[item.key][i].type =item.value;
              obj.push(data[item.key][i]); 
            }
            all_data.push(obj);
          });

          // incluir elementos ceros
          all_data.map(function(items_data){
            include_code =[];
            for (var i = 0; i < items_data.length; i++) {
              include_code.push(items_data[i].code);
            }
            var array3 = codes.filter(function(obj) { return include_code.indexOf(obj) == -1; });
            
            type = items_data[0].type;
            for (var i = 0; i < array3.length; i++) {
              new_obj = {code: array3[i], count:0, type:items_data[i].type}
              items_data.push(new_obj);
            }

          })


          console.log(all_data);
          resolve(all_data);
        });
      });
    }

    function render(all_data){

      var div = d3.select("body").append("div").attr("class", "tooltip_code");
      var margins = {top: 20, right: 150, bottom: 50, left: 40}
        legendPanel = {
          width: 240
        },
        width = 700 - margins.left - margins.right,
        height = 500 - margins.top - margins.bottom;

      var segmentsStacked = d3.keys(all_data[0]).filter(function(key){ return key !== "type"});


      stack = d3.layout.stack();

      dataset = makeData(segmentsStacked, all_data);

      function makeData(segmentsStacked, data) {
        segmentsStacked = d3.keys(all_data[0]).filter(function(key){ return key !== "type"});
        return segmentsStacked.map(function(component) {
          console.log(component);
          return data.map(function(d) {
            // console.log({x: d[0].type, y: +d[0].count, component: d[0].code});
            return {x: d[component].type, y: +d[component].count, component: d[component].code};
          })
        });
      }

      stack(dataset);

      var dataset = dataset.map(function(group) {
        return group.map(function(d) {
          return {
              x: d.y,
              y: d.x,
              x0: d.y0,
              component: d.component
          };
        });
      });

      console.log(dataset)
    svg = d3.select("#horizontal_chart_plot")
      .append('svg')
          .attr('width', width + margins.left + margins.right)
          .attr('height', height + margins.top + margins.bottom)
      .append('g')
          .attr('transform', 'translate(' + (margins.left + margins.bottom+10) + ',' + margins.top + ')'),
    xMax = d3.max(dataset, function(group) {
      return d3.max(group, function(d) {
          return d.x + d.x0;
      });
    }),
    xScale = d3.scale.linear()
        .domain([0, xMax])
        .range([0, width]),
    months = dataset[0].map(function(d) { 
      return d.y; 
    }),

    yScale = d3.scale.ordinal()
        .domain(months)
        .rangeRoundBands([0, height], .1),
    xAxis = d3.svg.axis()
        .scale(xScale)
        .orient('bottom'),
    yAxis = d3.svg.axis()
        .scale(yScale)
        .orient('left'),


    colours = d3.scale.category20(),
    groups = svg.selectAll('g')
      .data(dataset)
      .enter()
      .append('g')
        .attr("class", "cod_ic")
        .style('fill', function(d, i) {
          if(i < d.length){
            console.log(d[i].component);
            return colours(d[i].component);          
          }
        }),
    rects = groups.selectAll('rect')
      .data(function(d) { return d; })
      .enter()
        .append('rect')
          .attr('x', function(d) { return xScale(d.x0); })
          .attr('y', function(d, i) { return yScale(d.y); })
          .attr('height', function(d) { return yScale.rangeBand(); })
          .attr('width', function(d) { return xScale(d.x); })
          .attr("class", function(d){ return  "_" + d.component.replace(".", "_"); })
        .on("mousemove", function(d){
          div.style("left", d3.event.pageX+10+"px");
          div.style("top", d3.event.pageY-25+"px");
          div.style("display", "inline-block");
          div.html((d.y)+"<br>"+"Code"+": "+(d.component)+"<br>"+"Value"+": "+(d.x));
          show_current_type("_" + d.component.replace(".", "_"));
        })
        .on("mouseout", function(d){
          div.style("display", "none");
          show_all_active();
        });


    svg.append('g')
        .attr('class', 'axis')
        .call(yAxis);

    // Legend
    svg_legend = d3.select(".legend_value")
      .append('svg')
      .attr('width', 300)
      .attr('height', 350);

    svg_legend.append('rect')
        .attr('fill', 'transparent')
        .attr('width', 160)
        .attr('height', 30 * dataset.length)
        .attr('x', 0)
        .attr('y', 0);

    segmentsStacked.forEach(function(s, i) {
      svg_legend.append('text')
        .attr('fill', '#1e1e1e')
        .attr('x', 0 + 8)
        .attr('y', i * 24 + 24)
        .text(s);
      svg_legend.append('rect')
        .attr('fill', colours(i))
        .attr('width', 60)
        .attr('height', 20)
        .attr('x', 0 + margins.left + 30)
        .attr('y', i * 24 + 6);
    });


    // movement
    d3.selectAll("input").on("change", handleFormClick);

    function handleFormClick() {
      if (this.value === "percent") {
        percentClicked = true;
        transitionPercent();
      } else {
        percentClicked = false;
        transitionCount();
      }
    }

    function transitionPercent() {
      stack.offset("expand");  // use this to get it to be relative/normalized!
      var dataset = stack(makeData(segmentsStacked, all_data));
      var dataset = dataset.map(function(group) {
        return group.map(function(d) {
          return {
              x: d.y,
              y: d.x,
              x0: d.y0,
              component: d.component
          };
        });
      });
      transitionRects(dataset);
    }

    function transitionCount(){
      stack.offset("zero");
      var dataset = stack(makeData(segmentsStacked, all_data));
      var dataset = dataset.map(function(group) {
        return group.map(function(d) {
          return {
              x: d.y,
              y: d.x,
              x0: d.y0,
              component: d.component
          };
        });
      });
      transitionRects(dataset);
    }


    function transitionRects(dataset){
      xMax = d3.max(dataset, function(group) {
        return d3.max(group, function(d) {
            return d.x + d.x0;
        });
      });
      xScale = d3.scale.linear()
          .domain([0, xMax])
          .range([0, width]);
      months = dataset[0].map(function(d) { return d.y; });
      yScale = d3.scale.ordinal()
          .domain(months)
          .rangeRoundBands([0, height], .1);

      var code = svg.selectAll(".cod_ic")
        .data(dataset);

      code.selectAll("rect")
        .data(function(d) {
          return d;
        })
      svg.selectAll("g.cod_ic rect")
        .transition()
        .duration(250)
        .attr('x', function(d) { return xScale(d.x0); })
        .attr('y', function(d, i) { return yScale(d.y); })
        .attr('height', function(d) { return yScale.rangeBand(); })
        .attr('width', function(d) { return xScale(d.x); })

    }

    function show_all_active(){
      d3.selectAll("g.cod_ic rect").attr("style", "fill-opacity:1");
    }




    function show_current_type(current_code){
      d3.selectAll("g.cod_ic rect").attr("style", "fill-opacity:0.2");
      d3.selectAll("."+current_code).attr("style", "fill-opacity:1");
    } 
  }
}


/////////////////////////////////////////  TABLE ///////////////////////////////////////////////////////////

function createTable(final_data, order){
    size = final_data[0].length;
    item =["Rank"];

    order.map(function(order_item){
      item.push(order_item.value)
    })
      var table = $('<table class="table"></table>');
      row = $('<tr></tr>');
      $.each(item, function(i, list_item) {
        var rowData = $('<td></td>').addClass('item_table_header').text(list_item);
        row.append(rowData);
      });
      table.append(row)
      for (var j = 0; j <size; j++){
        row = $('<tr></tr>');
        var rowData = $('<td></td>').addClass('item_table').text(j + 1);
        row.append(rowData);
        $.each(order, function(i, order_item){
          var rowData = $('<td></td>').addClass('item_table').text(final_data[order_item.key][j].code +"("+ final_data[order_item.key][j].count +")");
          row.append(rowData);
          // final_data[order_item.key][j]
        })
        table.append(row);
      }
      $('.table_history').append(table);

}













  //  horizontalBar(); 



  // function horizontalBar(current_url){
  //   var div = d3.select("body").append("div").attr("class", "tooltip_code");
  //   var div_plot = document.getElementById("horizontal_chart_plot");
  //   var width = div_plot.clientWidth;
  //   var height = 0.5 * width;
  //   var color = d3.scale.category10();
  //   var margin = {top: 20, right: 50, bottom: 30, left: 50};


  //  current_url = "http://bdapp1.ccs.miami.edu/uride/icd9_history/401.9?limit=5"; 

  //   createPlot(current_url)
  //   function createPlot(){
  //     url_json = current_url;
  //     load(url_json).then(function(result){
  //       render(nuevos);
  //       console.log("Success plot");
  //     }).catch(function(){
  //       console.log("Error!");
  //     });
  //   }

  //   function load(url){
  //     datos =[];
  //     return new Promise(function(resolve, reject){
  //       $.getJSON(url, {get_param: 'value'}, function(data){
  //         var order = [">12m_b" , "6-12m_b", "<6m_b", "0", "<6m_a",  "6-12m_a", ">12m_a" ]

  //         nuevos = []
  //         order.map(function(item){
  //           var nuevo_obj = {};
  //           total_obj = data[item];
  //           nuevo_obj.label = item;
  //           total_obj.map(function(elem){
  //             nuevo_obj[elem.code] = elem.count;
  //           });
  //           nuevos.push(nuevo_obj);
  //         });
  //         // console.log(nuevos);
  //         resolve(nuevos);
  //       });
  //     });
  //   }

  //   function render(nuevos){
  //     // console.log(datos)

  //     options = [];
  //     $.each(nuevos, function(i, item){
  //       var keys_items = d3.keys(item).filter(function(key){ return key !== "label"});
  //       $.each(keys_items, function(j, elem){
  //         if ($.inArray(elem, options) === -1){
  //           options.push(elem);
  //         }
  //       });
  //     });

  //     var number_layers = d3.keys(nuevos[0]).filter(function(key){ return key != "label"}).length;
  //     var sample_per_layer = nuevos.length;
  //     var stack = d3.layout.stack();
  //     var labels = datos.map(function(d) {return d.label;});

  //     var values = [];
  //     $.each(nuevos, function(i, item_datos){
  //     var keys_items = d3.keys(item_datos).filter(function(key){ return key !== "label"});
  //       keys_items.map(function(opc){
  //         // a[i] = {x: i, y: nuevos[i][opc]};
  //         values.push({x: i, y:nuevos[i][opc] , code: opc, label:nuevos[i].label });
  //       });
  //     });


  //   var staked = [];
  //   for (var i = 0; i < sample_per_layer; i++){
  //     current_obj = d3.values(values).filter(function(key){ return key.x == i})
  //     for (var j = 0; j < number_layers; j++){
  //       var a={};
  //       if (j == 0){
  //         y0 = 0;
  //         current_obj[j].x = i;
  //         current_obj[j].y0 = y0;
  //         current_obj[j].tipo = j;
  //       }else{
  //         current_obj[j].x = i;
  //         current_obj[j].y0 = current_obj[j-1].y;
  //         current_obj[j].tipo = j;
  //       }
  //       staked.push(current_obj[j]);
  //     }
  //   }

  //   layers =[];
  //   for (var i = 0; i < number_layers; i++){
  //     current_layer = d3.values(staked).filter(function(key){ return key.tipo == i})
  //     layers.push(current_layer);
  //   }

  //   // the largest layer
  //   var yGroupMax = d3.max(layers, function(layer){ 
  //     return d3.max(layer, function(d){ return d.y; 
  //     }); });
  //   var yStackMax = d3.max(layers, function(layer){ 
  //     return d3.max(layer, function(d){ return d.y0 + d.y; 
  //     }); })



  //   var y = d3.scale.ordinal()
  //     .domain(d3.range(sample_per_layer))
  //     .rangeRoundBands([2, height], .08);

  //   var x = d3.scale.linear()
  //     .domain([0, yStackMax])
  //     .range([0, width]);

  //   var svg = d3.select(div_plot).append("svg")
  //       .attr("width", width + margin.left + margin.right)
  //       .attr("height", height + margin.top + margin.bottom)
  //     .append("g")
  //       .attr("transform", "translate(" + margin.left + "," + margin.top +")");


  //   var layer = svg.selectAll(".layer")
  //       .data(layers)
  //     .enter().append("g")
  //       .attr("class", "layer")
  //       .style("fill", function(d,i){ 
  //         return color(i); 
  //       });

  //   layer.selectAll("rect")
  //     .data(function(d) {
  //       return d; 
  //     })
  //     .enter().append("rect")
  //     .attr("y", function(d){ 
  //       return y(d.x); 
  //     })
  //     .attr("x", function(d){ 
  //       return x(d.y0);
  //     })
  //     .attr("height", y.rangeBand()/2)
  //     .attr("width", function(d){ 
  //       return x(d.y)
  //     })
  //     .on("mousemove", function(d){
  //       div.style("left", d3.event.pageX+10+"px");
  //       div.style("top", d3.event.pageY-25+"px");
  //       div.style("display", "inline-block");
  //       div.html((d.label)+"<br>"+"Code"+": "+(d.code)+"<br>"+"Value"+": "+(d.y));
  //     })
  //     .on("mouseout", function(d){
  //       div.style("display", "none");
  //     });

  //   }
  // }