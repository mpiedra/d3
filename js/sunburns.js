var num_click = 0;
var current_proyect = "";
var type = "";
var current_year = "";
url_json = "";
var obj_datos = [];
var datos = {name:"root"};
var text = "Num of jobs";
var totalSize = 0;

var chartDiv = document.getElementById("new_chart");

var width = chartDiv.clientWidth;
var height = .5 * width;

var radius = Math.min(width, height)/2;

var color = d3.scale.category10();

var x = d3.scale.linear()
    .range([0,2* Math.PI ]);

var y = d3.scale.sqrt()
    .range([0, radius]);


var svg = d3.select("#new_chart").append("svg")
    .attr("width", width)
    .attr("height", height)
    .append("g")
    .attr("transform", "translate("+ width/2 + "," + (height/ 2) + ")");

var partition = d3.layout.partition()
    .value(function(d){
      return d.size;
    });

var arc = d3.svg.arc()
    .startAngle(function(d){ return Math.max(0, Math.min(2 * Math.PI, x(d.x))); })
    .endAngle  (function(d){ return Math.max(0, Math.min(2 * Math.PI, x(d.x + d.dx))); })
    .innerRadius(function(d){return Math.max(0, y(d.y)); })
    .outerRadius(function(d){return Math.max(0, y(d.y + d.dy));});


createChart();



function createChart(){
  switch (num_click){
    case 0:
      url_json = "http://bdapp1.ccs.miami.edu/hpcjob/report/yp?columns=num_jobs";
      break;
    case 1:
      url_json = "http://bdapp1.ccs.miami.edu/hpcjob/report/ypu?values="+ current_year +"&columns=cpueff_max,cpuhour_tot,num_jobs";
      break;
      // var url_json = "http://bdapp1.ccs.miami.edu/hpcjob/report/ypu?values="+ current_year +"&columns=cpueff_max,cpuhour_tot,num_jobs";
    case 2:
      url_json = "http://bdapp1.ccs.miami.edu/hpcjob/report/ypuq?values=" + current_year + "," + current_proyect + "&columns=cpueff_max,cpuhour_tot,num_jobs";
      console.log(url_json);
      break;
    default:
      alert("Done");
      return;
  }
  // var datos = {name:"root"};
  obj_datos = []
  load(url_json).then(function(result){
    console.log(datos);
    render(datos);
    console.log("termine");
  }).catch(function(){
   console.log("ha habido un error!");
  });

}


function load(url){
 return new Promise(function(resolve, reject){
   $.getJSON(url, {get_param: 'value'}, function(data){
      $.each(data, function(key, value){
        var new_obj_year = {};
        var new_children = [];
        new_obj_year.name = key;
        $.map(value, function(obj){
          var new_obj_proj = {};
          new_obj_proj.name = obj.key;
          new_obj_proj.size = obj.num_jobs;
          new_children.push(new_obj_proj);
        });
        new_obj_year.children = new_children;
        obj_datos.push(new_obj_year);
      });
      datos = {name:"root"};
      datos.children = obj_datos;
      resolve(datos);
   });
 });
}


function render(items){
  var root = items;
  svg.append("svg:text")
    .attr("class", "description")
    .attr("text-anchor", "middle")
    .html(createText);

  var gs = svg.selectAll("g").data(partition.nodes(root));
  var g = gs.enter().append("g")
    .on("mouseover", mostrar)
    .on("click", click);
  var path = g.append("path");

  gs.select("path")
    .style("fill", function(d){ 
      if (d.depth != 0)
        return color((d.children ? d : d.parent).name); 
      else
        return "transparent";
    })
    .transition().duration(500)
    .attr("d", arc);
    if (path.node() != null)
      totalSize = path.node().__data__.value;

  gs.exit().transition().duration(500).style("fill-opacity", 1e-6).remove();



}


function click(d){
  num_click = num_click + 1;
  if (num_click == 1){
    current_year = d.depth == 1 ? d.name : d.parent.name;
    // if (d.depth != 1){
    //   var percentage = (100 * d.size / totalSize).toPrecision(2)
    //   var percentageString = percentage + "%";
    //   if (percentage < 0.1) {
    //     percentageString = "< 0.1%";
    //   }
    //   text += "<br/> Year:" + current_year + "<br/>" + percentageString + "of" + totalSize;
    // }else{
    //   text += "<br/> Year:" + current_year + "<br/>" + totalSize;
    // }
    // d3.select(".description")
    //   .html(text);
    console.log(current_year);
  }else{
    if (num_click == 2){
      current_proyect = d.depth == 1 ? d.name : d.parent.name;
      console.log(current_proyect);
    }
  }

  createChart();
}



function mostrar(d){
  if (d.depth !=0)
    console.log(d.depth == 1 ? d.name : d.parent.name);
}

function createText(){
  return text;
}


 // window.addEventListener("resize", redraw);