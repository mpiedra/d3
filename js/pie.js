// url_json = "http://bdapp1.ccs.miami.edu/hpcjob/report/yp?columns=num_jobs";
// datos = {name:"root", children:[]}

// function load(url){
//  return new Promise(function(resolve, reject){
//    $.getJSON(url, {get_param: 'value'}, function(data){
//      $.each(data, function(key, value){
//        alert(key);
//      });
//    });
//  });
// }

// load(url_json).then(function(result){
//  console.log("waiting for the data");
// }).catch(function(){
//  console.log("ha habido un error!");
// });


datos = [
  {"age": "<5", "population":2704659 },
  {"age": "5-13", "population":4499890 },
  {"age": "14-17", "population":2159981 },
  {"age": "18-24", "population":3853788 },
  {"age": "24-44", "population":3853788 },
  {"age": "45-64", "population":8819342 },
  {"age": ">=65", "population":612463 }
]



var width = 300,
    height = 300,
    radius = Math.min(width, height)/2;

//cambiamos la escala
var color = d3.scale.category10();

var arc = d3.svg.arc()
    .outerRadius(radius -70)
    .innerRadius(radius -30);


var labelArc = d3.svg.arc()
    .outerRadius(radius-40)
    .innerRadius(radius -40);


var pie = d3.layout.pie()
    .sort(null)
    // separar  las porciones 
    .padAngle(.02)
    .value(function(d){ return d.population; });

var svg = d3.select(".demo").append("svg")
    .attr("width", width)
    .attr("height", height)
  .append("g")
    .attr("transform", "translate(" + width/2 + "," + height/2 + ")");

render(datos);

function render(datos){

 var g = svg.selectAll(".arc")
    .data(pie(datos))
  .enter().append("g")
    .attr("class", "arc")


  g.append("path")
    .attr("d", arc)
    // .on("mouseover", arcTween(arc.outerRadius, 0))
    // .on("mouseout", arcTween(arc.outerRadius - 20, 150))
    .style("fill", function(d){return color(d.data.population)})

  g.append("text")
    .attr("transform", function(d){return "translate("+ labelArc.centroid(d) +")";})
    .attr("dy", ".35em")
    .text(function(d){
      return d.data.age;
    });


  // function arcTween(outerRadius, delay){
  //   return function() {
  //     d3.select(this).transition().delay(delay).attrTween("d", function(d) {
  //       var i = d3.interpolate(d.outerRadius, outerRadius);
  //       return function(t) { d.outerRadius = i(t); return arc(d); };
  //     });
  //   };
  // }

}


























