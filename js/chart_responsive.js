$(function () {
    var update_job = {};
    var update_cpu = {};
    var update_pending = {};


    function update(datos){
      current_date = datos.ts;
      dateFormat = current_date.substr(0,4) +'-'+ current_date.substr(4,2) +'-'+ current_date.substr(6,2) + 'T' + current_date.substr(8,2) + ':'+ current_date.substr(10,2) + ':' + current_date.substr(12,2);
      date = new Date(dateFormat);
      localTime = date.getTime();

      update_job.x = localTime;
      update_job.y = datos.jRUN;

      update_cpu.x = localTime;
      update_cpu.y = datos.sRUN;

      update_pending.x = localTime;
      update_pending.y = datos.jPEND;
    }

    function updateData(data_type){
      var current_url = "http://bdapp1.ccs.miami.edu/hpcjob/cj_stat/current";
      var opc = data_type;
      var dataset = [];
      $.ajax({
        data : {'type': opc},
        url : current_url,
        type : 'get',
        success: function(data){
          update(data)
        }
      });
      console.log("update data"); 
      console.log (update_job);
      console.log (update_cpu);
      console.log (update_pending);
    }

    function date_d3(current_date){
      dateFormat = current_date.substr(0,4) +'-'+ current_date.substr(4,2) +'-'+ current_date.substr(6,2) + 'T' + current_date.substr(8,2) + ':'+ current_date.substr(10,2) + ':' + current_date.substr(12,2);
      date = new Date(dateFormat);
      localOffset = date.getTimezoneOffset() * 60000;
      localTime = date.getTime();
      local_period = localTime + localOffset;
      final_date = new Date(local_period);
      return localTime;
    } 

    $.getJSON('http://bdapp1.ccs.miami.edu/hpcjob/cj_stat/history?minutes=60', function (data) {

        var types = ["jRUN"]; 
        // var types = ["jPEND", "jRUN", "sPEND", "sRUN", "sSUSP", "sRSV", "sNJOBS"]; 
        var info = [];
        data.forEach(function(item_data){
          types.forEach(function(type){
            var new_obj = [];
            fecha = date_d3(item_data["ts"]);
            new_obj.push(fecha)
            new_obj.push(item_data[type])
            // var time = date_d3(item_data["ts"]);
            // new_obj.time = time.getMinutes();
            info.push(new_obj);
          });
        });


        Highcharts.chart('container', {
            chart: {
                zoomType: 'x',
                backgroundColor: null,
                events:{
                    load:function(){
                        var series = this.series[0];
                        setInterval(function () {
                            var new_data = updateData("jRUN");
                            var x = update_job.x, // current time
                                y = update_job.y;
                            series.addPoint([x, y], true, true);
                        }, 60000);
                    }
                }
            },
            title: {
                text: 'Jobs Running',
                style:{
                  color: '#ffffff'
                }
            },
            colors: ['white'],
            subtitle: {
                text: document.ontouchstart === undefined ?
                        '' : ''
            },
            xAxis: {
                type: 'datetime',
                style:{
                  color: '#ffffff'
                }
            },
            yAxis: {
                title: {
                    text: ''
                }
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            exporting: {
              enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, 'rgba(255, 255, 255, 0,3)'],
                            [0,'rgba(255, 255, 255, 0.3)']
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                }
            },

            series: [{
                type: 'area',
                name: 'Job Running',
                data: info
            }]
        });
    });

//  CONTAINER 1
    $.getJSON('http://bdapp1.ccs.miami.edu/hpcjob/cj_stat/history?minutes=60', function (data) {

        var types = ["sRUN"]; 
        // var types = ["jPEND", "jRUN", "sPEND", "sRUN", "sSUSP", "sRSV", "sNJOBS"]; 
        var info = [];
        data.forEach(function(item_data){
          types.forEach(function(type){
            var new_obj = [];
            fecha = date_d3(item_data["ts"]);
            new_obj.push(fecha)
            new_obj.push(item_data[type])
            // var time = date_d3(item_data["ts"]);
            // new_obj.time = time.getMinutes();
            info.push(new_obj);
          });
        });


        Highcharts.chart('container1', {
            chart: {
                zoomType: 'x',
                backgroundColor: null,
                events:{
                    load:function(){
                        var series = this.series[0];
                        setInterval(function () {
                            var new_data = updateData("sRUN");
                            var x = update_cpu.x, // current time
                                y = update_cpu.y;
                            series.addPoint([x, y], true, true);
                        }, 60000);
                    }
                }
            },
            title: {
                text: 'CPUs Running',
                style:{
                  color: '#ffffff'
                }
            },
            colors: ['white'],
            subtitle: {
                text: document.ontouchstart === undefined ?
                        '' : ''
            },
            xAxis: {
                type: 'datetime',
                style:{
                  color: '#ffffff'
                }
            },
            yAxis: {
                title: {
                    text: ''
                }
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            exporting: {
              enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, 'rgba(255, 255, 255, 0,3)'],
                            [0,'rgba(255, 255, 255, 0.3)']
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                }
            },

            series: [{
                type: 'area',
                name: 'CPUs Running',
                data: info
            }]
        });
    });



//  CONTAINER 2
    $.getJSON('http://bdapp1.ccs.miami.edu/hpcjob/cj_stat/history?minutes=60', function (data) {

        var types = ["jPEND"]; 
        // var types = ["jPEND", "jRUN", "sPEND", "sRUN", "sSUSP", "sRSV", "sNJOBS"]; 
        var info = [];
        data.forEach(function(item_data){
          types.forEach(function(type){
            var new_obj = [];
            fecha = date_d3(item_data["ts"]);
            new_obj.push(fecha)
            new_obj.push(item_data[type])
            // var time = date_d3(item_data["ts"]);
            // new_obj.time = time.getMinutes();
            info.push(new_obj);
          });
        });


        Highcharts.chart('container2', {
            chart: {
                zoomType: 'x',
                backgroundColor: null,
                events:{
                    load:function(){
                        var series = this.series[0];
                        setInterval(function () {
                            var new_data = updateData("jPEND");
                            var x = update_pending.x, // current time
                                y = update_pending.y;
                            series.addPoint([x, y], true, true);
                        }, 60000);
                    }
                }
            },
            title: {
                text: 'Jobs Pending',
                style:{
                  color: '#ffffff'
                }
            },
            colors: ['white'],
            subtitle: {
                text: document.ontouchstart === undefined ?
                        '' : ''
            },
            xAxis: {
                type: 'datetime',
                style:{
                  color: '#ffffff'
                }
            },
            yAxis: {
                title: {
                    text: ''
                }
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            exporting: {
              enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, 'rgba(255, 255, 255, 0,3)'],
                            [0,'rgba(255, 255, 255, 0.3)']
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                }
            },

            series: [{
                type: 'area',
                name: 'Jobs Pending',
                data: info
            }]
        });
    });

});

  