// var datos_anidados = {"name": "root", 
// "children":[
//     {"name": "<5", "population":2704659 },
//     {"name": "5-13", "population":4499890 },
//     {"name": "14-17", "population":2159981 },
//     {"name": "18-24", "population":3853788 },
//     {"name": "24-44", "population":3853788 },
//     {"name": "45-64", "population":8819342 },
//     {"name": ">=65", "population":612463 }
//   ]
// }


var num_clicks = 0;
var opc = "";
var url_json = "";
var current_year = "";
var project = "";
var user = "";

if ((num_clicks == 0) && (!opc)){
  url_json = "http://bdapp1.ccs.miami.edu/hpcjob/report/yp?columns=num_jobs";
  opc = "year";
}else{
  if ((num_clicks == 1) && (opc === "year")){
    var current_url = "http://bdapp1.ccs.miami.edu/hpcjob/report/ypu?values="+ current_year +"&columns=cpueff_max,cpuhour_tot,num_jobs";
    opc = "project";
  }
}
var datos = {name:"root"};
var obj_datos = [];
function load(url){
 return new Promise(function(resolve, reject){
   $.getJSON(url, {get_param: 'value'}, function(data){
      $.each(data, function(key, value){
        var new_obj_year = {};
        var new_children = [];
        new_obj_year.name = key;
        $.map(value, function(obj){
          var new_obj_proj = {};
          new_obj_proj.name = obj.key;
          new_obj_proj.size = obj.num_jobs;
          new_children.push(new_obj_proj);
        });
        new_obj_year.children = new_children;
        obj_datos.push(new_obj_year);
      });
      datos.children = obj_datos;
      resolve(datos);
   });
 });
}


load(url_json).then(function(result){
  console.log(datos);
  render(datos);
}).catch(function(){
 console.log("ha habido un error!");
});


function reload(){
  if ((num_clicks == 0) && (!opc)){
    url_json = "http://bdapp1.ccs.miami.edu/hpcjob/report/yp?columns=num_jobs";
    opc = "year";
  }else{
    if ((num_clicks == 1) && (opc === "year")){
      var url_json = "http://bdapp1.ccs.miami.edu/hpcjob/report/ypu?values="+ current_year +"&columns=cpueff_max,cpuhour_tot,num_jobs";
      opc = "project";
    }
  }
  var datos = {name:"root"};
  var obj_datos = [];
  load(url_json).then(function(result){
    console.log(datos);
    render(datos);
  }).catch(function(){
   console.log("ha habido un error!");
  });
}

var width = 300,
    height = .7 * width,
    radius = Math.min(width, height)/2;

var color = d3.scale.category10();

var x = d3.scale.linear()
    .range([0,2* Math.PI ]);

var y = d3.scale.sqrt()
    .range([0, radius]);


var svg = d3.select(".new_chart").append("svg")
    .attr("width", width)
    .attr("height", height)
    .append("g")
    .attr("transform", "translate("+ width/2 + "," + (height/ 2) + ")");

var partition = d3.layout.partition()
    .value(function(d){
      return d.size;
    });

var arc = d3.svg.arc()
    .startAngle(function(d){ return Math.max(0, Math.min(2 * Math.PI, x(d.x))); })
    .endAngle  (function(d){ return Math.max(0, Math.min(2 * Math.PI, x(d.x + d.dx))); })
    .innerRadius(function(d){return Math.max(0, y(d.y)); })
    .outerRadius(function(d){return Math.max(0, y(d.y + d.dy));});

function render(items){
  var root = items;
  var gs = svg.selectAll("g").data(partition.nodes(root));
  var g = gs.enter().append("g")
    .on("click", click);
  var path = g.append("path");

  gs.select("path")
    .style("fill", function(d){ 
      return color((d.children ? d : d.parent).name); 
    })
    .transition().duration(500)
    .attr("d", arc);
  gs.exit().transition().duration(500).style("fill-opacity", 1e-6).remove();
}


function click(d){
  num_clicks ++;
  if (num_clicks == 1){
    current_year = d.depth == 1 ? d.name : d.parent.name;
    opc = "year";
    reload();
  }
  
  // console.log(d);
}

// render(datos_anidados);
